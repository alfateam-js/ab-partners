const nodemailer = require('nodemailer');
const csv = require('csv');
const iconv = require('iconv-lite');

const defaults = {
    "smtp": {
        "host": "smtp.host",
        "port": 25
    },
    "email": {
        "from": "bicc@alfabank.kiev.ua",
        "to": "maksym.pascal@alfabank.kiev.ua",
        "encoding": "base64"
    },
    "csv": {
        "delimiter": ";",
        "header": true,
        "quoted": true,
        "columns": [
            "optyId",
            "url",
            "createdISO",
            "distributedISO",
            "last_name",
            "fst_name",
            "mid_name",
            "phoneNumber"
        ]
    }
}
const transporter = nodemailer.createTransport(defaults.smtp);

const getRandom = (min = 1000, max = 9999) => {
    return Math.floor(Math.random() * (max - min)) + min;
}
const getCSV = (data) => {
    return new Promise((resolve, reject) => {
        data.forEach(row => {
            row.createdISO = new Date(row.created).toISOString();
            row.distributedISO = new Date(row.distributed).toISOString();
        })
        csv.stringify(data, defaults.csv, (err, output) => {
            if (err) reject(err);
            resolve(output);
        });
    })
}
const getTestRow = () => {
    const id = getRandom();
    return {
        "optyId": `1-SOMEID${id}`,
        "created": new Date().getTime(),
        "msgId": `SOMEIDSOMEIDSOMEIDSOMEIDSOMEID${id}`,
        "clientId": `2-SOMEID${id}`,
        "last_name": `ПАСКАЛ${id}`,
        "fst_name": `МАКСИМ${id}`,
        "mid_name": `ИВАНОВИЧ${id}`,
        "phoneNumber": `+38000000${id}`,
        "landing": "alfabank.ua",
        "distributed": new Date().getTime(),
        "type": "PartnersDistribution:PARTNER:InternetOnline",
        "url": `https://cfront.alfabank.kiev.ua/?msgId=SOMEIDSOMEIDSOMEIDSOMEIDSOMEID${id}`,
        "ActionID": "ok"
    }
}

let data = [];
for (let a = 0; a < 20; a++) {
    data.push(getTestRow());
}

['json-as-body', 'json', 'csv', 'no-data'].forEach(async integration => {
    let mailOptions = Object.assign({}, defaults.email);

    mailOptions.subject = `integration=${integration}`;

    switch (integration) {
        case 'csv':
            const csvData = await getCSV(data);
            mailOptions.attachments = [{
                filename: 'data.csv',
                content: iconv.encode(new Buffer(csvData), "win1251")
            }];
            break;
        case 'json':
            mailOptions.attachments = [{
                filename: 'data.json',
                content: JSON.stringify(data)
            }];
            break;
        case 'json-as-body':
            mailOptions.text = JSON.stringify(data);
            break;
        case 'no-data':
            mailOptions.text = 'no data';
            break;
        default:
            console.error('unknown integration');
    }

    transporter.sendMail(mailOptions, (err, info) => {
        if (err) console.error(err);
    });
})